<!-- Note from mkdocs: "If both an indx.md and a README.md are in the same directorfy, mkdocs will render the index file" and the README will still be rendered on github (https://www.mkdocs.org/user-guide/writing-your-docs/). Adding frontmatter to README breaks it." -->

[![DOI:10.5281/zenodo.7463254](https://zenodo.org/badge/DOI/10.5281/zenodo.7463254.svg)](https://doi.org/10.5281/zenodo.7463254)


Thank you for visiting the WIN Open Neuroimaging Community project repository.

**This is the GitLab README**

This page is a hub to give you some information about the Community and our Open Science Ambassadors. Jump straight to one of the sections below, or scroll down to find out more.

* [What are we doing? (And why?)](#what-are-we-doing)
* [Who are we?](#who-are-we)
* [What do we need?](#what-do-we-need)
* [How can you get involved?](#get-involved)
* [Get in touch](#contact-us)
* [Find out more](#find-out-more)
<!-- * [Understand the jargon](#glossary) -->

## What are we doing?

We are a community built from around 250 members of the University of Oxford Wellcome Centre for Integrative Neuroimaging (WIN). Our community exists to support open and inclusive researchers, to develop the tools, policies, governance and structures we would like in place to support us in practicing open science.

We area committed to supporting transparent and reproducible research practices within WIN, to improve the efficiency and accuracy of our research, and contribute to an equitable, diverse and inclusive research environment.

### The problem

Funders and the Management Board of WIN have identified the value of open science and invested in computational infrastructure to support researchers in sharing their data, data acquisition methodology, experimental tools and analysis code, in an open, transparent and reproducible manner.

As these stages near completion, we are entering the second phase of work to build awareness, provide training and increase uptake in the use of this infrastructure. The problem is that there are no centralised resources, communication structures or identified community strategies in place to support researchers in using this new infrastructure.

### The solution

The WIN Open Neuroimaging Community will address the above problems by working together to: (1) Develop tools, policies and governance recommendations to promote open research practices; (2) Identify our own barriers to working open and devising strategies to remove these barriers for ourselves and others; (3) Build a community of researchers with the expertise and confidence to promote open research practices in our own labs and to wider audiences.

We intend to implement these solutions through regular co-working "hacks". These hacks will be full day events where we will discuss, write and build together. A high value outcome of these hacks will be a sense of community among the researchers who attend. All material created during the hack will be openly shared on this repository. We will also generate materials and guidance about how we have created the hacks, so these events are themselves reproducible.

Alongside the hacks, we will develop the WIN Open Ambassadors program, where motivated individuals will be supported in sharing knowledge and sign posting their peers around open research practices and how they are implemented at WIN. The Ambassadors will be recognised for the value of their contribution, and invited to shape the policies, incentives, guiding principles and reward systems which will be devised to promote uptake of open research practices at WIN.

### Why here?

We are building our resources and documenting our growth using this version controlled git repository, rendered into an accessible "book" format. Using git makes it easy to review and accept contributions and flexibly update with a full history of activity. It also provides a starting point for researchers who are unfamiliar with git to practice contributing to a repository and gain confidence using the git process. This is an essential skill in open and reproducible research, an excellent skill to have on your CV, and one which will be vital to using some parts of the WIN Open Infrastructure. We are therefore pleased to offer all our contributors training and support in git throughout their engagement with the project. You will find training and linked resources for using git in our [contributing guide](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/CONTRIBUTING.html).

## Who are we?

### Open Neuroimaging Community Co-Ordinator - Cassandra Gould van Praag (she/her)

Cass is a postdoctoral researcher with 10 years of neuroimaging (fMRI) research. She was Co-Chair of the [Open Science Room 2020](https://ohbm.github.io/osr2020/), and has been both a participant and speaker at the [Berlin-Oxford Open Science Summer School](https://www.nds.ox.ac.uk/events/oxford-berlin-summer-school-on-open-research-2019), an invited speaker at [Open MR Benelux](https://openmrbenelux.github.io/) and is a Fellow of [Reproducible Research Oxford](https://ox.ukrn.org/). Cass is an active contributor to a number of open community tools, including [Open Research Calendar](https://openresearchcalendar.github.io/Open-Research-Calendar/), the [COBIDAS Checklist](https://github.com/Remi-Gau/COBIDAS_chckls) and [The Turing Way](https://the-turing-way.netlify.com/introduction/introduction)

Email: Cassandra.GoudVanPraag@psych.ox.ac.uk  
GitHub: @cassgvp  
Twitter: @cassgvp

### Open WIN Steering Group

This community feeds directly into the [WIN Open Neuroimaging Project](https://www.win.ox.ac.uk/open-neuroimaging/open-neuroimaging-project) and its working groups listed below along with their leads.

* Open Data (Clare Mackay)
* Open Tools (Mark Jenkinson)
* Open Protocols (Stuart Clare)
* Open Paradigms (Laurence Hunt)

You can find out more about the WIN Open Neuroimaging Project by joining this [mailing list](https://web.maillist.ox.ac.uk/ox/info/win-open-imaging)


### Contributors
Please see our [CITATION.cff](CITATION.cff) for a list of all those who have contributed directly to the written materials contained in this repository.

## What do we need?

**You**! We need people to join the community! If you would like to learn more about open research, become an [Ambassador](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/ambassadors.html) or join one of our hacks, please join the WIN Open Neuroimaging Project [mailing list](https://web.maillist.ox.ac.uk/ox/info/win-open-imaging), and stay tuned for further announcements.

Please note that it's very important to us that we maintain a positive and supportive environment for everyone who wants to participate. When you join us we ask that you follow our [code of conduct](CODE_OF_CONDUCT.md) in all interactions both on and offline.


## Contact us

If you want to ask a question, report a problem or suggest an enhancement we'd love to hear from you! Please take a look at our [CONTRIBUTING](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/CONTRIBUTING/) guide for ways to get in touch.


## Citing this repository

Please cite as:

Open WIN Community, Gould van Praag, Cassandra, Clarke, William, Cottaar, Michiel, Draschkow, Dejan, Feng, Yingshi, McCarthy, Paul, Sarrazin, Verena, Taschler, Bernd, MacKay, Clare, & Clare, Stuart. (2022). Open WIN Community Pages. Zenodo. [https://doi.org/10.5281/zenodo.7463254](https://doi.org/10.5281/zenodo.7463254)
