# This CITATION.cff file was generated with cffinit.
# Visit https://bit.ly/cffinit to generate yours today!

cff-version: 1.2.0
title: Open WIN Community Pages
message: >-
  If you use this software, please cite it using the
  metadata from this file.
type: software
authors:
  - given-names:
    family-names: Open WIN Community
    email:
    affiliation: University of Oxford
    orcid:
  - given-names: Cassandra
    family-names: Gould van Praag
    email: cassandra.gouldvanpraag@psych.ox.ac.uk
    affiliation: University of Oxford
    orcid: 'https://orcid.org/0000-0002-8584-4637'
  - given-names: William
    family-names: Clarke
    email: william.clarke@ndcn.ox.ac.uk
    affiliation: University of Oxford
    orcid: 'https://orcid.org/0000-0001-7159-7025'
  - given-names: Michiel
    family-names: Cottaar
    email: michiel.cottaar@ndcn.ox.ac.uk
    affiliation: University of Oxford
    orcid: 'https://orcid.org/0000-0003-4679-7724'
  - given-names: Dejan
    family-names: Draschkow
    email: dejan.draschkow@psy.ox.ac.uk
    affiliation: University of Oxford
    orcid: 'https://orcid.org/0000-0003-1354-4835'
  - given-names: Yingshi
    family-names: Feng
    email: yingshi.feng@seh.ox.ac.uk
    affiliation: University of Oxford
    orcid: 'https://orcid.org/0000-0001-9065-4945'
  - given-names: Paul
    family-names: McCarthy
    email: pauldmccarthy@gmail.com
    affiliation: University of Oxford
  - given-names: Verena
    family-names: Sarrazin
    email: verena.sarrazin@balliol.ox.ac.uk
    affiliation: 'University of Oxford '
    orcid: 'https://orcid.org/0000-0002-5796-5378'
  - given-names: Bernd
    family-names: Taschler
    email: bernd.taschler@ndcn.ox.ac.uk
    affiliation: University of Oxford
    orcid: 'https://orcid.org/0000-0001-6574-4789'
  - given-names: Clare
    family-names: MacKay
    email: clare.mackay@psych.ox.ac.uk
    affiliation: University of Oxford
    orcid: 'https://orcid.org/0000-0001-6111-8318'
  - given-names: Stuart
    family-names: Clare
    email: stuart.clare@ndcn.ox.ac.uk
    affiliation: University of Oxford
    orcid: 'https://orcid.org/0000-0001-6421-9990'
  - given-names:
    family-names: Open WIN Community
    email:
    affiliation: University of Oxford
    orcid:
identifiers:
  - type: doi
    value: 10.5281/zenodo.7463254
repository-code: >-
  https://git.fmrib.ox.ac.uk/open-science/community/Open-WIN-Community
url: >-
  https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/
abstract: >-
  This repository is the hub for information on how
  to employ open research practices at the Wellcome
  Centre for Integrative Neuroimaging (WIN).
  Describes how to share research outputs effectively
  and responsibly, for improved impact, access and
  collaboration. The material shared here is written
  and maintained by an active community of WIN
  researchers, and is intended to support all WIN
  members in using the sharing infrastructure created
  by the WIN Open Neuroimaging Project. Named authors
  are those who have contributed directly to the
  written material.
keywords:
  - open science
  - neuroimaging
  - reproducibility
  - MRI
  - MEG
  - brain
license: CC-BY-4.0
version: '1.0'
date-released: '2022-12-20'
