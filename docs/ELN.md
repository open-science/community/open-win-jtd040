---
layout: default
title: Electronic lab notebooks
has_children: false
nav_exclude: true
nav_order: 1
---

# Lab Archives electronic lab notebooks
{: .fs-9 }

sub heading
{: .fs-6 .fw-300 }

---

![tools](../../img/logo-labarchives.png)

Laboratory notebooks document the process of experimentation and discovery, and may be maintained as a legal document admissible as evidence in court of law  (https://en.wikipedia.org/wiki/Electronic_lab_notebook). As such, there is a drive to promote the use of electronic laboratory notebooks (ELNs) over paper lab notebooks to improve fidelity, access and interoperability of this valuable information. Standards have been developed by the International Organization for Standardization (ISO) and the International Electrotechnical Commission (IEC) to describe minimum requirements in controlling access and editing of entries into an ELN; ELNs providers can apply to be accredited under a given ISO/IEC specification, giving users confidence that the information they enter into them is managed appropriately.

We will use an ELN to document the processes we go through in conducting our MRI analysis. Your ELN will exist as a version controlled diary of your activity and findings. You can choose to keep this information as a personal record for yourself or share with colleagues as necessary. You will see that an ELN is preferable to a continually editable document (e.g. and MS Word document) for this purpose specifically because you cannot edit old entries. You can therefore use an ELN to improve the reproducibility of your research by recording what you step-by-step, allowing you to reproduce effects by following the same step-wise procedure at a later date.

We will make use of the University of Oxford’s subscription to the LabArchives ELN service which is ISO/IEC 27001 compliant (https://help.it.ox.ac.uk/research/eln/infosec). Under these provisions you have access to a free account with unlimited storage (individual files are limited to a maximum of 250 MB). Notebooks can be shared internally to another Oxford employee with an SSO account, or in a ‘view only’ format to external collaborators (see here for further information: https://help.it.ox.ac.uk/research/eln/faqs). Notebooks cannot be shared with open (unrestricted) viewing. Be mindful of the extent to which your ELN can be shared when entering data into it.
Set up your lab archives account by following the instructions provided on the IT Services Quick Start Guide and familiarise yourself with the different entry types available:  https://help.it.ox.ac.uk/sites/ithelp/files/resources/research_LabArchivesQuickStartGuide.pdf

Your LabArchives account will automatically be populated with a number of ‘template’ pages. As an initial suggestion, you may wish to create a new folder named as “MyProject_ImageAnalysisLog”, and have a file under each aligned with each part of the analysis process as outlined in this guide (e.g. “Part01-ToolsForReproducibility”).

You should describe your activity and decision making processes on each page in whatever way feels appropriate for you. You can include figures (screen shots), plain text, rich text or tables. Try to cultivate a practice of updating your notes every time you interact with the data or make a decision, while the memory is fresh, rather than waiting to record your activity at the end of the day. Save each entry to the page at regular intervals. You are using this tool primarily as an aid for your own memory, so there is no need to be too concerned with presentation. Do be mindful, however, that you may be asked to share these notes with a colleague and they should therefore be appropriate for a professional environment.

You may wish to explore what LabArchives can offer to see how we can use it most effectively. Try writing in plain text using markdown notation (https://en.wikipedia.org/wiki/Markdown#Example). Training videos and online seminars specific to LabArchives are available to Oxford users – make use of these and share your findings with the group! For example, would it be useful to create a widget for reviewing dicoms?!

We suggest that you begin using your ELN now, and document everything which follows in this guide. This will include a record of any software you have to install or additional resources you have to access. This point therefore denotes that start of your analysis journey.
