---
layout: default
title: Open Tasks
has_children: true
nav_order: 3
---


# Open Tasks
{: .fs-9 }

How to share your experimental tasks
{: .fs-6 .fw-300 }

---

![open-tasks](../../img/img-open-tasks-flow.png)

## Purpose
The Open Tasks working group aims to encourage and incentivise researchers designing experimental tasks for functional neuroimaging to share these tasks openly with other researchers. They have established online repository for these paradigms to be shared, alongside documentation and analysis scripts for behavioural results. This working group also aims to train new members of the centre in open-source coding languages for paradigm development, and sharing reproducible environments.

<br>

<!-- [![For WIN members](../../img/btn-win.png)](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/tools/tasks/#for-win-members)      [![For external researchers](../../img/btn-external.png)](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/tools/tasks/#for-external-researchers) -->

[![For WIN members](../../img/btn-win.png)](../tasks/tasks_guide_internal-how-1)      [![For external researchers](../../img/btn-external.png)](../tasks/tasks_guide_external)

## Contributors
We are grateful to the following WIN members for their contributions to developing the Open MR Protocols database
- [Dejan Draschkow](https://www.psych.ox.ac.uk/team/dejan-draschkow)
- [Irene Echeverria Altuna](https://www.psy.ox.ac.uk/team/irene-echeverria-altuna)
- [Amy Gillespie](https://www.psych.ox.ac.uk/team/amy-gillespie)
- [Cassandra Gould van Praag](https://www.win.ox.ac.uk/people/cassandra-gould-van-praag)
- [Cameron Hassall](https://www.cameronhassall.com/)
- [Laurence Hunt](https://www.win.ox.ac.uk/people/laurence-hunt)
- [Paula Kaanders](https://uk.linkedin.com/in/paulakaanders)
- [Verena Sarrazin](https://www.psych.ox.ac.uk/team/verena-sarrazin)
- [Suyi Zhang](http://www.seymourlab.com/people/suyi-zhang.html)
