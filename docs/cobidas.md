---
layout: default
title: COBIDAS
has_children: false
nav_exclude: true
nav_order: 1
---

# COBIDAS methods reporting
{: .fs-9 }

sub heading
{: .fs-6 .fw-300 }

---

In reporting our research, we should aim to align as far as possible with the suggestions made by the Committee on Best Practice in Data Analysis and Sharing (COBIDAS). This committee was formed by the Organization for Human Brain Mapping; the leading community representing brain research involving MRI and other imaging modalities (https://www.humanbrainmapping.org/i4a/pages/index.cfm?pageid=3728). The COBIDAS report was produced to address a need for the unification in reporting of brain MRI research, to support high level of transparency and reproducibility in our field.  Achieving COBIDAS levels of reporting requires close attention to be paid to the analytic methods employed in your research, and the specifics of data collection. At the outset, this necessitates the collation of materials describing your research process into an easily accessible and version controlled repository, such as OSF.
