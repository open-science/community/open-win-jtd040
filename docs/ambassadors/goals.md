---
layout: default
title: Goals of the program
parent: Open WIN Ambassadors
grand_parent: Open WIN Community
has_children: false
nav_order: 2
---

# What are the goals of the Ambassador program?
{: .fs-9 }

Find out about why we are running the Ambassador programme and what we hope to achieve.
{: .fs-6 .fw-300 }

---

* [Aims and objectives of the Ambassador programme](#aims-and-objectives-of-the-ambassador-programme)
  - [1. Growing our user base](#1-growing-our-user-base)
  - [2. Improving the transparency, reproducibility and integrity of our research](#2-improving-the-transparency,-reproducibility-and-integrity-of-our-research)
  - [3. Building capacity in open and collaborative leadership](#3-building-capacity-in-open-and-collaborative-leadership)
  - [4. Setting the agenda](#4-setting-the-agenda)
  - [5. Shaping culture](#5-shaping-culture)
* [Evaluation of the programme and your experience](#evaluation-of-the-programme-and-your-experience)
  - [Success against our objectives](#success-against-our-objectives)
  - [Delivery of the programme](#delivery-of-the-programme)
  - [Impact going forwards](#impact-going-forwards)

The Open WIN Ambassadors programme is an essential component in achieving the vision of the Open WIN Community:

> The Open WIN Community will empower WIN researchers to share all forms of research output, through the provision of training, skills development, and contribution to open science policy.

> We are working open as this is an effective way to develop a user focused and inclusive project which all members of our community can contribute to and benefit from.

## Aims and objectives of the Ambassador programme

### 1. Growing our user base

Our overarching goal is to increase the number of users of our [sharing infrastructure](../../tools), so more people can share all stages of their research output, and receive appropriate credit for doing so. This will help us achieve the Open Neuroimaging Project goal of increasing the openness of our research, to facilitate discovery and accelerate the translation of methods and results to the clinic.

One of the most effective ways of growing our user base is to capitalise on the skills, reach and influence of a select group of "power users", who can advocate for local change and represent the interests of their colleagues. These power users (our Ambassadors) will have the best understanding of what others needs to see, feel, and experience in their journey in order to successfully adopt an effective open science practice.

**As part of the programme we would like you to contribute to the documentation which will guide your colleagues in using the Open WIN infrastructure. These resources will become the go-to for new users, and some cases the stepping off point in their open science journey. It is important that we set the right standard and expectation from their first interaction with the project. We'd also like you to give a talk about open science or the Open WIN infrastructure to your lab, and think about what practices could be incorporated into your routine workflow.**

### 2. Improving the transparency, reproducibility and integrity of our research

As reported by the [Software Sustainability Institute](https://www.software.ac.uk/news/major-funding-boost-uks-open-research-agenda?mc_cid=0d68e9a951&mc_eid=8c2ea6a0db), "*The benefits of open research practices for improving the quality and integrity of research have been widely documented, and are recognised by the [UK Government R&D Roadmap](https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/896799/UK_Research_and_Development_Roadmap.pdf) as contributing to improving the culture of research*"

The Ambassadors will have access to a curated list seminars and workshops which will teach core skills in transparent and reproducible research for neuroimaging. This will arm them with the knowledge and expertise needed to include transparent and reproducible workflows in their own research.

**By the end of the programme we expect our Ambassadors to be experts in using the Open WIN tools, have used GitLab to publish training material with appropriate accreditation, and where possible have openly shared one of their own research outputs.**

### 3. Building capacity in open and collaborative leadership

The Ambassador programme will create opportunities for our community to gain experience in open and inclusive leadership, project management, collaboration and community building. These skills are an essential contributor to positive research culture, and opportunities to practice and reflect on these skills can have a positive impact on career trajectories.

**Alongside individual career development, building capacity in open leadership will have a knock-on effect in the broader open science landscape within and outside of WIN. We expect our Ambassadors will go on to lead their own open projects, and become key contributors to open science communities and projects in the wider neuroimaging community.**


### 4. Setting the agenda

One of the most difficult and unresolved problems in open science is how we appropriately incentivise good open science practice, and what policy (local or otherwise) we should have in place to support and reward open science. For example, should we include open science in professional development reviews? Should we explore open science "badges" on our work or profiles? How should we track engagement and growth in open science outputs? These topics all deserve careful consideration, not least because inappropriate use of metrics and "gaming the system" is a large contributor to the problems which open science seeks to address.

**As people who will be directly impacted by policy introduced to incentivise or monitor open science activities at WIN, we would like the Open WIN Ambassadors to help devise, consult on and explore how we move forward to promote behaviour change.**


### 5. Shaping culture

In all our activities, we will aim to model a positive research culture. Following extensive survey of researchers across all career stages, The Wellcome Trust identified common themes in what researchers describe as [essential characteristics for a positive research culture](https://wellcome.org/sites/default/files/what-researchers-think-about-the-culture-they-work-in.pdf):

- diversity is encouraged and celebrated
- collaboration is encouraged and celebrated
- individual contributions feel valuable and valued
- individuals feel supported
- individuals feel safe and secure
- leadership is transparent and open
- time to think is valued.

We believe that in order to feel safe, secure and valued, we should cultivate an environment where the "whole person" is welcomed and respected. We recognise that we are more than just a "researcher", or "student", and that the collection of our parts is what makes us unique in our contributions. We have hopes, fears, passions, disagreements, strengths, weaknesses, relationships, responsibilities, pain, joys, sadness, laughter and complexities, and we cannot be expected to (always) separate these things from how we work. Recognising our individual diversity also enables us to be cognisant of our own biases and blind spots, and work to minimise the impact of them. Sharing ourselves as whole people, when we feel safe to do so, provides space for us all to support, accommodate and celebrate together.

**You will be expected to participate in the Ambassadors programme with the intention to embody a positive research culture, where we are respectful and supportive of each other as whole people. You will be expected to interact with honesty, humility and compassion towards yourself and others at all  times.**


## Evaluation of the programme and your experience

We intend to closely follow your experience and see how well we are meeting our aims and objectives. We would also like to know how effective the programme has been in meeting your needs and expectations, how it could be improved, and the impact you think it will have going forwards.

The results of the 2021-2022 evaluation will be shared at the 2022-2023 launch meeting and with the Open WIN Steering Group. A summary will be published on these pages.


### Success against our objectives

We will take a baseline measure of your open science activities at the start, and revisit this as the programme draws to a close. Success against our objectives will be operationalised as an increase from baseline in the proportion of "yes" answers to the below questions from our Ambassadors:

1. Have you created and published some documentation on an Open WIN tool?
2. Have you started a new open/community project or contributed to an existing one (internal to WIN or an external project)?
3. Have you used our infrastructure for one or more of your research outputs?
4. Have you done another open science thing outside of a topic we have directly trained on, for example shared a prepreint or pre-registered a study?
5. Have you given a talk about open science of the Open WIN tools to your lab?
6. Has your lab incorporated any open science practice into your "routine work flow"?
7. Has someone come to you with a question about open science? Were you able to answer it or did you know where to get support?
8. Have you contributed to WIN open science policy?

### Delivery of the programme

At the end of each training session and the end of the programme, we would like your feedback on what has worked well and what could be improved:

1. What worked well?
2. What surprised you?
3. What would you change?
4. What would you like to know more about?

### Impact going forwards

At the close of the programme, we will collect feedback on your predicted long-term impact of the course, including our attempts to embody a positive research culture.

1. What (if any) part of the programme has had the greatest impact on your research practice?
2. What (if any) part of the programme has impacted your career development?
3. What (if any) opportunities would you like to be available to continue your engagement with the Open WIN Community?

#### Culture

1. Do you feel as though diversity was encouraged and celebrated?
2. Do you feel as though collaboration was encouraged and celebrated?
3. Did individual contributions feel valuable and valued?
4. Did you feel supported?
5. Did you feel safe and secure?
6. Was leadership transparent and open?
7. Was time to think valued?
8. Did you feel welcomed and respected as a whole person?



<!--






We have specific goals for engagement which are driven by the project metrics which communicated to the [External Advisory Board (EAB)](https://sharepoint.nexus.ox.ac.uk/sites/NDCN/FMRIB/Shared%20Documents/EAB_TOR.pdf#search=external%20review%20board) who are tasked with providing "an external and independent view on plans and progress" of WIN. <mark> Can the 2018 EAB report be shared on intranet too? - yes. </mark>.

We also have a goal to show a year-on-year growth in understanding and engagement with open research practices, as measured by the [WIN 2019 Open Science Survey](https://sharepoint.nexus.ox.ac.uk/sites/NDCN/FMRIB/SitePages/WIN%20Survey%20Results%202019.aspx) and the

Based on these combined drivers, our initial goals are to:

## Demonstrate infrastructure engagement

Open Data
- Number of WIN researchers with database accounts
- Percentage of WIN researchers actively using the database
- Number of scans in the database
- Number of studies in the database
- Number of studies shared across WIN
- Number of studies shared externally
- Number of external requests to access studies


Open Paradigms
- Number of paradigms published on the website, potentially number of visits to the site
- Uplift in supporting documentation standards <mark>Added by CGVP. Much of the paradigms repo is pretty near unusable as so little documentation.</mark>

Open Protocols
- Number of protocols openly published in database
- Number of visits to the database and number of downloads
- Number of citations/acknowledgements of protocols used from the database

Open Tools
- Number of FSL tools that have singularity outputs
- Number of downloads of tools with singularity outputs
- Number of publications using singularity outputs
- Number of BIDS meetings attended

Open Community
<mark>TBC and consider more</mark>
- Number of Ambassadors
- Number of users on mailing lists and number of messages <mark>Can we look back at this historically?</mark>
- Number of users (active and inactive) on slack
- Number of contributors to this repository
- Number of enquires (and responses) made to Ambassadors
- Number of attendees at hack-day
- Feedback from hack-day

## Improve awareness and experience with open research practices
- Increase in the number of open access papers immediately available
- Improve awareness of open research practices, particularly registered reports and open educational materials.
- Encourage the community to share pre-prints of their papers.


![OS-Survey 2019 slide 3](../../img/open-science-survey-results/Slide3.png)

- Appreciate the value of open science practices in career development

![OS-Survey 2019 slide 4](../../img/open-science-survey-results/Slide4.png)

- Appreciate the value of open sharing of code.
- Encourage the community to realise the value of sharing data within the current limits of our [open data policy](OpenData.md), and equip them with the tools and knowledge to do so.

![OS-Survey 2019 slide 5](../../img/open-science-survey-results/Slide5.png)

## Reduce barriers to engaging in open research practices

- Devise and promote positive incentives for open research within WIN.
- Clarify and communicate legal and ethical obligations fo data sharing.
- Promote and train the community to use the newly developed WIN Open Infrastructure for sharing [data](OpenData.md), [experimental paradigms](OpenParadigms.md), [MR protocols](OpenProtocols.md) and [analysis tools](OpenTools.md)

![OS-Survey 2019 slide 6](../../img/open-science-survey-results/Slide6.png)

![OS-Survey 2019 slide 7](../../img/open-science-survey-results/Slide7.png)

## Create and share documentation
- Empower the community to learn together as we explore these topics, and share their knowledge on this repository <mark>update when moved</mark> in the form of written documentation.
- Hold in-person and online training workshops and "hacks" where we can learn together and create documentation.

![OS-Survey 2019 slide 8](../../img/open-science-survey-results/Slide8.png) -->
