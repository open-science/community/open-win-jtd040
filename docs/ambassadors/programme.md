---
layout: default
title: Programme schedule
parent: Open WIN Ambassadors
grand_parent: Open WIN Community
has_children: false
nav_order: 6
---
# Programme schedule
{: .fs-9 }

Find out about how the Ambassadors programme will be structured and scheduled

{: .fs-6 .fw-300 }

---

**Contents**
- [Technology](#technology)
- [Call structure](#call-structure)
- [Shadowing](#shadowing)
- [Schedule](#schedule)


All activities of the Ambassadors programme will take place online. Any in-person celebratory or team building meetings will be designed to be as accessible as possible and accommodating to the responsibilities of our Ambassadors.

## Technology

We will use a number of online technologies to work efficiently, transparently and reproducibly online. Many of these solutions will provide you with transferable skills which you will be able to incorporate into your own research workflow. *Training and support will be provided to ensure all Ambassadors are comfortable working in this way.*

Calls will take place on MS Teams.

We will use [hackmd.io](http://hackmd.io/) to write collaborative notes during the call. You will need a [GitHub](http://github.com/) account to use hackmd. Take a look at the [notes from our 2021-2022 meetings](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/tree/master/call-notes).

We will use the [WIN GitLab instance](http://git.fmrib.ox.ac.uk/) to write documentation and manage project work. You will require a WIN IT account to access GitLab.

We may use [padlet](https://padlet.com) for creative exercises.

## Call structure

#### Calls 1:4

During the first month of the programme you will participate in workshops to help you work with git and GitLab, understand more about the value and practices of open science, learn how to use the Open WIN tools.

#### Calls 5:6

Once you are familiar with the Open WIN infrastructure and working on GitLab, you will be encouraged to decide which tool(s) you would like to develop documentation for. You may also have ideas about which other projects you would like to take on or contribute to. In weeks 5 we will allocate the tools and discuss [what materials we need to create to support uptake](https://git.fmrib.ox.ac.uk/open-science/community/Open-WIN-Community/-/issues/25). In week 6 we will set up your GitLab space to develop documentation and your own projects.

#### Calls 8:15

Our regular calls will be 90 minutes long; 30 minutes for updates and problem solving, then an optional 60 minutes for co-working. These calls will be held on the first Wednesday of the month.

During the update time, we will first silently document updates together, then read each others updates and pose questions. This process is intended to make efficient use of our time as you will not have to prepare updates in advance or sit and wait for your turn to report back. If anyone would like support, guidance, to address questions from others or seek opinions on any part of their work, they will be invited to open the floor for feedback.

During the 60 minute co-working time we will break out into small flexible groups and work on documentation and/or our open and community projects. This explicit co-working time is scheduled so you can have a dedicated time and space to contribute. You may choose to contribute outside of this time, or arrange co-working times which better suite your needs.

## Shadowing

In response to feedback from the 2021-2022 Ambassadors, we have incorporated various opportunities for you to shadow the work of our Community Engagement Coordinator, so you can learn about the operational aspects of doing open science and supporting the wider community. You will be invited to attend the termly [Open WIN Steering Group](../../community/community-who/#open-win-steering-group) meetings, join one-to-one support meetings with researchers seeking to incorporate open science practices in their work, and join a team inbox.

## Launch and close meetings

At the start of the programme you will join the Ambassadors of previous years for a formal introduction to the Open WIN (delivered by the [Open WIN Steering Group](../../community/community-who#open-win-steering-group), hear the results of our [programme evaluation](../goals/#evaluation-of-the-programme-and-your-experience) and share your expectations and wishes for the year ahead. These activities will be held at a College in early November 2022, in an informal environment where we can celebrate each others success and contributions.

## Schedule

Below is a guideline schedule of activities for the 2022-2023 cohort. Calendar invitations for each meeting will be sent to all Ambassadors.

We recognise that these activities will sit alongside your usual commitments. Will endeavour to be responsive to your work schedule.

| Call | Date | Time | Topic  |
|---|---|---|---|
| 1 | Wednesday 9th November 2022 | 09:30 - 11:00 | intro to the programme |
| 2 | Wednesday 16th November 2022 | 09:30 - 11:00 | git and gitlab |
| 3 | Wednesday 23rd November 2022 | 09:30 - 11:00 | tools demos and questions (meet the developers) |
| 4 | Wednesday 30th November 2022 | 09:30 - 11:00 | policy, incentives, training, monitoring |
| 5 | Wednesday 7th December 2022 | 09:30 - 11:00 | getting into teams, setting up project space |
| 6 | Wednesday 14th December 2022 | 09:30 - 11:00 | getting into teams, setting up project space |
| 7 | Wednesday 4th January 2023 | 09:30 - 11:00 | update and co-working |
| 8 | Wednesday 1st February 2023 | 09:30 - 11:00 | update and co-working |
| 9 | Wednesday 1st March 2023 | 09:30 - 11:00 | Mid point review |
| 10 | Wednesday 5th April 2023 | 09:30 - 11:00 | update and co-working |
| 11 | Wednesday 3rd May 2023 | 09:30 - 11:00 | update and co-working |
| 12 | Wednesday 7th June 2023 | 09:30 - 11:00 | update and co-working |
| 13 | Wednesday 5th July 2023 | 09:30 - 11:00 | update and co-working |
| 14 | Wednesday 2nd August 2023 | 09:30 - 11:00 | update and co-working |
| 15 | Wednesday 6th September 2022 | 09:30 - 11:00 | close out and feedback |
