---
layout: default
title: Meet the Ambassadors
parent: Open WIN Ambassadors
grand_parent: Open WIN Community
has_children: false
nav_order: 1
---

# Meet the Ambassadors
{: .fs-9 }

Find out about the current Open WIN Ambassadors
{: .fs-6 .fw-300 }

---

## The 2022-2023 Ambassadors

![Open WIN Ambassadors 2023](../../../img/img-ambassadors-2023-horizontal.png)
Open WIN Ambassadors 2022-2023 (left-right): Peter Doohan; Miguel Farinha; Julian Fars; Lisa Spiering; Mohamed Tachrount.
{: .fs-3 .fw-300 }

### Peter Doohan

Peter is a DPhil Student in the Nuffiled Department of Clinical Neuroscience.

*"I want to applied to be an Open WIN Ambassador to help connect researchers across WIN, Oxford and the global neuroscience community to maximise the impact of the science done at WIN."*

### Miguel Farinha

Miguel is a DPhil Student in the Department of Psychiatry.

*"As an avid user of open-source software/data, I am very enthusiastic about the possibility of contributing to the widespread use of open research practices across the WIN community and the wider scientific community. "*

### Julian Fars

Julian is a Postdoctoral Researcher in the Nuffiled Department of Clinical Neuroscience. More about Julian on their [WIN profile page](https://www.win.ox.ac.uk/people/julien-fars)

*"I always wanted to make my analyses, data and experiment freely accessible for other research teams. Making is easy for others to reproduce my results are extremely important. "*

### Lisa Spiering

Lisa is a DPhil Student in the Department of Expereimental Psychology. More about Lisa on her [Department profile page](https://www.psy.ox.ac.uk/people/lisa-spiering)

*"I am very keen to learn more about the Open WIN tools that are available to us at WIN in order to make our research more transparent and replicable. I am also interested in learning more about how to promote an open and inclusive research culture. I've noticed that more researchers in my lab are interested in making their projects more transparent and open; so I am hoping that by becoming an Ambassador I can also help others to be more proficient in open science practices."*

### Mohamed Tachrount

Mohamed is a Senior Physics Support Scientist (Clinical and Pre-clinical) in the Nuffiled Department of Clinical Neuroscience. More about Mohammed in his [Department profile page](https://www.ndcn.ox.ac.uk/team/mohamed-tachrount)

*"I would like to learn how to make our scientific research more transparent, accessible, reproducible, and robust so any experiment (including protocols and data) can be checked, replicated, and extended by the community."*

-------

## The 2021-2022 Ambassadors

![Open WIN Ambassadors 2022](../../../img/img-ambassadors-2022-horizontal.png)
Open WIN Ambassadors 2021-2022 (left-right): Dejan Draschkow; Yingshi Feng; Verena Sarrazin; Bernd Taschler.
{: .fs-3 .fw-300 }

### Dejan Draschkow

Dejan is a Departmental Lecturer at the Department of Experimental Psychology. See his profile [here](https://www.psy.ox.ac.uk/people/dejan-draschkow).

*"I applied to the Ambassadors programme to learn about WIN’s open science activity, provide a bridge to the work happening in the Department of Experimental Psychology and help disseminate best practice. The most valuable component so far has been staying in touch with Cass and staying up-to-date on the work that is being done."*

### Yingshi Feng

Yingshi is a DPhil student in WIN Plasticity Group. See her profile [here](https://www.win.ox.ac.uk/people/yingshi-feng).

*"I applied to the Ambassadors programme to learn more about strategies for making scientific research more replicable, rigorous, and accessible. I have gained skills in using data sharing tools and an awareness of the broad beneficial impact open science practices have on individual researchers, research groups, collaborators, and the general public."*

### Verena Sarrazin

Verena is a DPhil student in the Psychiatry Department. See her profile [here](https://www.psych.ox.ac.uk/team/verena-sarrazin).

*"I applied to the Ambassador programme because I wanted to meet other people who are interested in open science, discuss challenges and future directions, and help make implementation of open science as easy as possible for researchers. I found it very useful to learn to use git because it is so transferrable, and it was interesting to shift from “consumer” to “contributor” in developing documentation. I now feel more confident starting conversations about open science and suggesting that others to publish all of their research outputs."*

### Bernd Taschler

Bernd is a postdoc in WIN's Analysis Group. See his profile [here](https://www.win.ox.ac.uk/people/bernd-taschler).

*"I wanted to take part in the Ambassadors programme to make a direct contribution to changing the way we do and incentivise research. Working on a larger project as a team has been valuable to help practice my communication and collaborative development skills. I’m now more convinced than ever that open science practices will be fundamental for how we can improve our current research culture.”*
