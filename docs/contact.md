---
layout: default
title: Join us on Slack or by Email
parent: Home
has_children: false
nav_order: 6
---

# Contact Us
{: .fs-9 }

Connect with us via any of the means below as per your preference and interests
{: .fs-6 .fw-300 }

---

## WIN members and external researchers

### Email the Open WIN Community Coordinator directly ![mailto](../../img/icon-at.png)

You are very welcome to [email Cassandra Gould van Praag](mailto:cassandra.gouldvanpraag@psych.ox.ac.uk) directly to discuss any issue relating to open research at WIN, the community, or this repository.

---

## WIN members only

### Open WIN Slack ![slack](../../img/icon-slack.png)

Anyone affiliated with a WIN member Department is invited to [join our Slack workspace](https://join.slack.com/t/openwin/signup). We have channels dedicated to each of the [sharing tools](../tools), the [Open WIN community](../community), and relevant external and internal [events](../events).

**Slack is where the conversation will be most active and we will share resources with each other. If you're new to the Community, this should be your first contact point!**

The above [slack invite link](https://join.slack.com/t/openwin/signup) will accept email addresses from any of the University of Oxford domains listed below. If you think your domain should be listed here, or you are a WIN member without an email address at one of these domains, please email the [Open WIN Community Coordinator](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/contact/#email-the-open-win-community-coordinator-directly-).

#### WIN member domains (alphabetically)
- [bdi.ox.ac.uk](https://www.bdi.ox.ac.uk)
- [eng.ox.ac.uk](https://eng.ox.ac.uk)
- [fmrib.ox.ac.uk](https://www.win.ox.ac.uk/about/locations/fmrib)
- [ndcn.ox.ac.uk](https://www.ndcn.ox.ac.uk)
- [ohba.ox.ac.uk](https://www.win.ox.ac.uk/about/locations/ohba)
- [paediatrics.ox.ac.uk](https://www.paediatrics.ox.ac.uk)
- [psy.ox.ac.uk](https://www.psy.ox.ac.uk)
- [psych.ox.ac.uk](https://www.psych.ox.ac.uk)
- [win.ox.ac.uk](https://www.win.ox.ac.uk)

### Open WIN mailing list ![mail-list](../../img/icon-maillist.png)

Anyone interested in the Open WIN Community or these resources can join the [WIN Open Neuroimaging mailing list](https://web.maillist.ox.ac.uk/ox/info/win-open-imaging). *The list is open only to University of Oxford staff and students (requires an SSO login)*. This mailing list is used for open distribution of information.

### Comment on the GitLab repository
<!-- ![github](../../img/icon-github.png) -->

If you want to report a problem or suggest an enhancement for this repository, we'd love for you to [open an issue](https://git.fmrib.ox.ac.uk/open-science/community/Open-WIN-Community/-/issues) on our GitLab page (requires a [WIN GitLab account](../gitlab/2-1-starting-gitlab-account).
