---
layout: default
title: Edit your protocol
parent: Open MR Protocols
has_children: false
nav_order: 4
---


# Editing your protocol entry
{: .fs-9 }


---

<!-- ![open-protocols](../../../img/img-open-mrprot-flow.png) -->


If you are using the protocols datbase to track changes made to your protocol while pilotting, you can amend the entry and create different version which can be reviewed at any point.

![gif - update](../../../img/img-protocols/gif_update.gif)

1. Go to `my protocols` and find the entry you would like to update.
2. Select whether you are adding a new sequence pdf (`Update with new protocol file`) or making a change to other parts of the entry only (`Update keeping existing protocol file`)
3. Identify whether you are making a minor amendment to descriptive text, incrementing the "edit" number only, or a more substantial update, incrementing the "version" number.
4. Make the required changes.
5. We recommend adding a "Change Log" section to the `Description` field, to record what changes were made and why.
6. Save your edits using the `save` button at the bottom of the page.
7. You can review your edits by clicking on `full history` on any protocol entry.

### Transferring ownership
If you are unable to log in with an SSO (your Oxford account has expired), you will need to transfer ownership of your protocol to someone else (potentially the Principal Investigator on the project). To arrange this, please contact admin@win.ox.ac.uk and request that the entry is transferred.
