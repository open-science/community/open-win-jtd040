---
layout: default
title: Why use the MR Protocols Database
parent: Open MR Protocols
has_children: false
nav_order: 1
---


# Why use the MR Protocols Database
{: .fs-9 }

Why you should consider using the WIN MR protocols database to record and share details of your MR data acquisition.
{: .fs-6 .fw-300 }

---

<!-- ![open-protocols](../../../img/img-open-mrprot-flow.png) -->


*The MR protocol database has been developed to share MR protocols internally across WIN as well as externally. The database provides access to standard protocols and to the latest experimental protocols uploaded by WIN members. The database facilitates sharing your MR sequence within and between research groups and helps you to keep track of updates using version control. Each deposited protocol contains all information necessary to reproduce that sequence on an appropriately licensed scanner, allowing yourself and others to have a permanent and full record of your data acquisition methodology.*


## Benefits
### Version control ![version-control](../../../img/icon-version-control.png)
The MR Protocols database is version controlled, so WIN members are invited to upload their protocols during piloting to keep track of optimisation and final versions. Comments and notes can be added to each entry to you can keep well structured documentation about why certain decisions were made. You can also link database entries with acquired data held on the [Open Data](../../data) server

### Citable research output ![doi](../../../img/icon-doi.png)
Versions (with minor and major edits) can be assigned a digital object identified (DOI) using the [Oxford Research Archive (ORA) Data repository](https://deposit.ora.ox.ac.uk) using the stable link provided by the MR Protocols database. Once a DOI has been created, your MR protocol becomes a citable object which you can add to your list of research outputs.

### Reproducible methods detail ![reproduce](../../../img/icon-reproduce.png)
Depositing your MR protocol in the database is as simple as uploading the sequence pdf available from the scanner console, or alternatively you can upload the .exar file used by our [Siemens scanners](https://www.win.ox.ac.uk/about/facilities). All sequence detail is captured from either document, and parsed into a unified machine readable and searchable format. You can upload a single sequence, or all sequences used in a single imaging session (a protocol) together.


## Who to share MR data acquisition with?
### Yourself and your research group
The MR Protocols database keeps track of changes to the protocol (for example sequences added, sequences modified in volume or parameters) for best practice in reproducibility and posterity. Maintaining your MR protocol in the database can help you keep an accessible record of what changes were made and why, for your record keeping and communication within your research group.


### Internally with other WIN members
The MR Protocols database provides full and reproducible information about the protocol efficiently, along with details of your decision making process (if used during piloting). You can effective share complete details of your protocol with another scan user.


### Externally with other researchers
By using the MR Protocols database:
1. You (and the radiographers) receive the credit for this part of the work (you can include citable DOIs)
2. Makes it easier to share this information with colleagues, potential collaborators, and included as supplementary materials in a publication.
3. Keeps a reproducible record for your lab of all details and decisions made (version control)
4. You contribute to open and reproducible research since other researchers might reuse your MR protocol. This will speed up translation of your research findings and will ultimately increase the impact of your research.
