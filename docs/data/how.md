---
layout: default
title: How do I share my data?
parent: Open Data
has_children: false
nav_order: 3
---


# How do I share my data?
{: .fs-9 }

Places you can share your data from
{: .fs-6 .fw-300 }

---

How you share your data (or where you share it from) will depend on the format of the data, the degree which you have been able to deidentify it, and your requirements for managed access or attribution.

Before sharing your data on any of these platforms, we suggest you:
1. Confirm your requirements for [governance](../can-i#governance), [ethics](../can-i#ethics) and [reuse](../can-i#appropriate-reuse).
2. [Deidentify](../can-i#deidentification) your data
3. Curate your [metadata](../can-i#metadata)

After these stages have been considered, one or a combination of the below [repositories](#repositories) may be appropriate for sharing your data.

## Contributor attribution
Before you publish your data you should have an open and honest conversation about who has contributed to the data collection and processing, and agree how these individuals will be recognised and attributed for their work. This need not be the same list of individuals who are authors on any manuscript which references your data, indeed this can be a valuable opportunity to recognise the contributions of those who do not traditionally receive authorship on journal manuscripts (for example project managers, software engineers or data stewards).

Consider creating a [Contributor Roles Taxonomy - CRediT](https://credit.niso.org) statement and sharing this with your published data. [Tenzing](https://rollercoaster.shinyapps.io/tenzing/) is a useful tool for collecting contributor information and generating the CRediT statement.

Aim to include the [ORCID](https://orcid.org) of all your contributors in your published metadata, so contributions can be traced back to the individual.

## Repositories
### Open Science Foundation (OSF)
Tabular or text data (for example behavioural results or derived region of interest values) can be shared very effectively from the [Open Science Foundation (OSF)](http://osf.io/). OSF has built in utilities for version control, reviewer only access links, generating a digital object identifier (doi) and licence, so your data can be cited and separately from any other output. You can also grant different levels of authoring access and identify biblographic contributors which will be included in the auto-generated citation template.

**OSF is not suitable for sharing data which requires managed access on a large scale. Individuals can be granted access, but large groups (for example "anyone with an ORICD") cannot be added in bulk.**

When using OSF we recommend creating an account using your ORCID, ensuring your ORCID is linked to your SSO (see [Already have an ORCID? link it to Oxford](https://libguides.bodleian.ox.ac.uk/ORCID/orcid/register)). Once your SSO and ORCID are linked (allow this to process overnight), you can add Oxford affiliation to your OSF projects via the `Settings` tab as shown below. Note currently your affiliation needs to be added to each project individually (there is no bulk add option).

![Add Oxford affiliation to your OSF projects via the "Settings" tab on OSF](../../../img/img-osf-sso-oxford.png)

Take a look at this recording from the WIN Graduate program to learn more about OSF.

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/668636108?h=35c6cf9f25&title=0&byline=0&portrait=0&speed=0&badge=0&autopause=0&player_id=0&app_id=58479/embed" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen frameborder="0" style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe></div>

### Zenodo
[Zenodo](https://zenodo.org) is excellent for many sorts of data, although there are file size limits.
Coming soon
{: .label .label-yellow }


### NeuroVault
[NeuroVault](https://neurovault.org) is an accessible resource for collecting and distributing statistical maps of the brain (any brain data that is stored in a 3D NIFTI format). This could include group level contrasts or participant level contrasts in group (or MNI) space. It is a simple tool for adding some additional transparency to your publication.

Data shared on NeuroVault can be given a persistent url identifier, but not a digital object identifier.

You should also be satisfied with the licence conditions of adding your data to NeuroVault, particularly that it can be reused for any purpose (including commercial) and that you will not receive attribution for re-use of your data.

### EBRAINS
[EBRAINS](https://ebrains.eu)
Coming soon
{: .label .label-yellow }

### OpenNeuro
[OpenNeuro](https://openneuro.org) is a well known public MRI data sharing platform. It is developed and hosted in the USA, and as such it does not make provisions for data handling as required by UK GDPR.

**If you would like to use OpenNeuro, we suggest you only share data in group space (for example MNI space) and not participant or native space, which remains identifiable. Be sure not to share the transform files which would enable group space data to be reverted to individual participant space.**

**You should be confident that you have removed as many identifiable features as possible from all parts of the data, including ancillary files.**

You should also be satisfied with the licence conditions of adding your data to OpenNeuro, particularly that it can be reused for any purpose (including commercial) and users are not required to provide attribution for re-use of your data. Users are however *encouraged* to cite data depositors as good academic practice.

Each version of a dataset added to OpenNeuro is assigned a unique DOI

### WIN Open Data Portal
The Open Data Working Group is building a searchable, user friendly Open Data Portal which will contain data transferred directly from the scanners (MRI and MEG) and associate it with other research data (for example behavuoural) for the same project. Image conversion tools are integrated into the database to convert raw image files to standard formats and the community standard [Brain Imaging Data Structure (BIDS)](https://bids.neuroimaging.io) file structures.

The Open Data Portal will provide managed access at the level prescribed by the researcher (for example fully open, verified researchers only, or by email invitation). You will also be able to specify details such as required authorship,  prohibition from attempts to re-identify participants, and any other terms of reuse you require.

Data shared on the Open Data Portal will be given a DOI and explicit citation text.

#### How to use
While the Open Data Portal is in development, we suggest you use the below wording in your data availability statement in journal submissions:

> "Deidentified data will be made available on the WIN Open Data Portal. This is currently in development. Register here to find out when materials are available for download: [https://web.maillist.ox.ac.uk/ox/subscribe/win-open-data](https://web.maillist.ox.ac.uk/ox/subscribe/win-open-data)"

#### For external researchers
External users will be able to search the database for resources which individual teams have chosen to make openly available. These may be deposited to support publications as supplementary methods material, or they may form the main body of research in data papers.

[Sign up to the win-open-data mailing list](https://web.maillist.ox.ac.uk/ox/subscribe/win-open-data) to receive updates on new data shared to the WIN XNAT server or new features. This mailing list will be "broadcast only" with messages sent from WIN XNAT administrators. You can unsubscribe at any point. Enquiries about external access to WIN XNAT should be directed to cassandra.gouldvanpraag@psych.ox.ac.uk.
