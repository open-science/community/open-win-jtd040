---
layout: default
title: Can I share my data?
parent: Open Data
has_children: false
nav_order: 2
has_toc: false
---


# Can I share my data?
{: .fs-9 }

Things to consider when planning to share your data
{: .fs-6 .fw-300 }

---

<!-- ![cani-all](../../../img/img-cani-all.png) -->



<!-- &nbsp; -->

[![cani-gov-sing](../../../img/img-cani-gov-sing.png)](#governance)
[![cani-ethics-sing](../../../img/img-cani-ethics-sing.png)](#ethics)
[![cani-deident-sing](../../../img/img-cani-deident-sing.png)](#deidentification)
[![cani-meta-sing](../../../img/img-cani-meta-sing.png)](#metadata)
[![cani-reuse-sing](../../../img/img-cani-reuse-sing.png)](#appropriate-reuse)

Click on any of the images above to jump to each section.

<!-- Key -->
<html>
<body><small>
<p><i class="fas fa-exclamation-triangle"></i> = essential to address before sharing your data.<br>
<i class="fas fa-check-circle"></i> = desirable best practice.</p>
</small></body>
</html>

## Governance

<!-- ![cani-gov](../../../img/img-cani-gov.png) -->
<!-- <html><body><i class="fas fa-exclamation-triangle"></i></body></html> -->
<!-- <html><body><i class="fas fa-check-circle"></i></body></html> -->

<details>
 <summary><html><body><i class="fas fa-exclamation-triangle"></i></body></html> <b>Is sharing restricted under Intellectual Properties rights?</b></summary><br>

 Intellectual property rights are rights granted to creators and owners of works that are the result of human intellectual creativity (<a href="https://www.jisc.ac.uk/guides/intellectual-property-rights-in-a-digital-world">JISC Guides</a>). Put simply, you first need to establish whether the data is yours to share. If you have acquired new data for this project, you are in control of how it is shared. If, however, you are conducting secondary analysis on data acquired from elsewhere, please check what limits the owners have imposed on re-sharing. This information may be available from their license or a data usage agreement which you confirmed when accessing the data.
 <br><br>
</details>

<details>
  <summary><html><body><i class="fas fa-exclamation-triangle"></i></body></html> <b>Has your funder or industry partner approved data sharing?</b></summary><br>

  If your research was partly or whole funded by an industry partner, they may have imposed conditions to restrict sharing to protect they commercial interests. Review any contracts to insure that funders or industry partners allow you to share the data.
  <br><br>
</details>

<details>
  <summary><html><body><i class="fas fa-exclamation-triangle"></i></body></html> <b>Have you investigated commercial potential of your data?</b></summary><br>

  As a University employee you are obliged to consider the commercial potential of your outputs. If you think there may be commercial value in your data, please speak to <a href="https://innovation.ox.ac.uk/university-members/commercialising-technology/ip-patents-licenses/oui-fit/">Oxford University Innovation (OUI)</a> for support.
  <br><br>
</details>

<details>
 <summary><html><body><i class="fas fa-check-circle"></i></body></html> <b>Have you discussed open data sharing in your data management plan?</b></summary><br>

 <p>Creating a data management plans helps you plan how you will manage the data acquired during your project by considering the type of data you are producing, who needs to access it and accordingly how it is stored. They are a mandatory part of some grant applications, but they are also a useful exercise for smaller projects which don't require separate funding.</p>

 <p>We have collected <a href="https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/data/data-management-plans/">Data Management Plans created for WIN projects</a> which can use as a guide for your own projects. The above page also links to University resources and relevant policy.</p>

<br><br>
</details>

<details>
 <summary><html><body><i class="fas fa-check-circle"></i></body></html> <b>Does your Data Protection Impact Assessment (DPIA) reference data sharing?</b></summary><br>

 All studies which collect new or re-use existing data must be assessed for risks of a data breach. This risk is assessed using a <a href="https://compliance.admin.ox.ac.uk/data-protection-forms#collapse1091641">Data Protection Impact Assessment (DPIA) Screening form</a>. Note for the purposes of the DPIA Screening, human imaging data is only considered "biometric data" ("personal data resulting from specific technical processing relating to the physical, physiological, or behavioural characteristics of a natural person, which allows or confirms the unique identification of that natural person, such as facial images or fingerprint data.") if you intended to run some sort of "matching" algorithm.<br><br>
</details>

<details>
 <summary><html><body><i class="fas fa-exclamation-triangle"></i></body></html> <b>Are you sharing data acquired from living humans?</b></summary><br>

 UK GDPR restrictions relate only to data acquired from living humans.

 <p><b>Non-human</b> data are not required to be de-identified. Consider sharing your data on the <a href="https://open.win.ox.ac.uk/DigitalBrainBank/#/">Digital Brain Bank</a>.</p>

 <p><b>Ex vivo human data</b> should be treated in accordance with the requirements of the Common Law Duty of Confidentiality. You should also be aware of the possibility of living individuals (for example relatives of the deceased) being identified in this information, which would then need to be treated in line with UK GDPR personal information. Please review the <a href="http://www.hra-decisiontools.org.uk/consent/principles-deceased.html">HRA Decision Tool for principles for handling data from deceased human participants</a>. Consider sharing your data on the <a href="https://open.win.ox.ac.uk/DigitalBrainBank/#/">Digital Brain Bank</a>.</p><br><br>
</details>

## Ethics

<details>
 <summary><html><body><i class="fas fa-exclamation-triangle"></i></body></html> <b>Have you described data sharing in your ethics application?</b></summary><br>

 Your ethics application and participant information sheet should minimally refer to the sharing of data with colleagues outside of the University. Ideally, you should include the possibility of sharing "deidentified data in online databases". If you have collected MRI data under <a href="https://researchsupport.admin.ox.ac.uk/governance/ethics/resources/ap#collapse397171">CUREC Approved Procedure 17 (version 6.0+)</a> a statement fulfilling this requirement will be included already.
 <br><br>
</details>

<details>
 <summary><html><body><i class="fas fa-exclamation-triangle"></i></body></html> <b>Has your participant consented to data sharing?</b></summary><br>

 Many consent forms have a separate section or box to indicate the participant is aware of your data sharing plans (box 4 on the <a href="https://researchsupport.admin.ox.ac.uk/governance/ethics/resources/ap#collapse397171">Approved Procedure 17 consent form</a>). Has your participant indicated they have agreed to data sharing as you have described?
 <br><br>
</details>

## Deidentification

<!-- ![cani-deident](../../../img/img-cani-deident.png) -->

<!-- <html><body><i class="fas fa-exclamation-triangle"></i></body></html> -->
<!-- <html><body><i class="fas fa-check-circle"></i></body></html> -->


<details>
 <summary><html><body><i class="fas fa-exclamation-triangle"></i></body></html> <b>Have you removed any "direct identifiers" in your data?</b></summary><br>

 Direct identifiers are things which identify an individual without any additional information. For example their name, address or telephone number. <i>This information should never be shared with the data</i>.
 <br><br>
</details>

<details>
 <summary><html><body><i class="fas fa-exclamation-triangle"></i></body></html> <b>Are your imaging data in participant space?</b></summary><br>

 Participant space (cortical structure) is unique to an individual and as such is an identifiable feature under UK GDPR. Avoid sharing data which is in participant space where possible. If it is preferable to share data in participant space, ensure other features described below are redacted as appropriate for your analysis.
 <br><br>
</details>

<details>
 <summary><html><body><i class="fas fa-exclamation-triangle"></i></body></html> <b>Have your Participant IDs been protected?</b></summary><br>

 Are any keys which link researcher generated Participant IDs and WIN generated Scan IDs to UK GDPR "special category" data (names, contact information, consent documentation etc.) held in a facility which is surrounded by a suitable regime of controls and safeguards to prevent data breaches and misuse (<a href="https://cronfa.swan.ac.uk/Record/cronfa53688">Jones and Ford, 2018</a>)? In practice, this is achieved by following <a href="https://researchsupport.admin.ox.ac.uk/files/bpg09datacollectionandmanagementpdf">CUREC BPG 09</a>, with data only held on a approved shared drive (Departmental or One Drive), or a device with whole disk encryption.

 The linkage key must be "stored separately from" (<a href="https://researchsupport.admin.ox.ac.uk/files/bpg09datacollectionandmanagementpdf">CUREC BPG 09</a>) special category and research data, It must not be shared with research data except in critical circumstances. The validity of requests for access to the linkage key should be assessed on an individual basis by the responsible data controller (usually the Principle Investigator).
 <br><br>
</details>

<details>
 <summary><html><body><i class="fas fa-exclamation-triangle"></i></body></html> <b>Have "indirect identifiers" such as age, gender, handedness or disease status been protected?</b></summary><br>

 Consider combining "indirect identifiers" (<a href="https://researchsupport.admin.ox.ac.uk/files/bpg09datacollectionandmanagementpdf">CUREC BPG 09</a>) into bins such that no participant can be uniquely identified. Ideally bins should contain >= 5 participants.
 <br><br>
</details>

<details>
 <summary><html><body><i class="fas fa-exclamation-triangle"></i></body></html> <b>Have unique dicom fields been scrubbed?</b></summary><br>

 If you are sharing dicom data, you should aim to scrub the dicom headers of identifiable and unique fields. Consider the relative risk of retaining some fields if they are important to your analysis.
 <br><br>
</details>

<details>
 <summary><html><body><i class="fas fa-exclamation-triangle"></i></body></html> <b>Have unique fields in .json sidecar files been scrubbed?</b></summary><br>

 If you are sharing nifti data with json sidecar files, you should scrub the .json files of all identifiable and unique fields.  Consider the relative risk of retaining some fields if they are important to your analysis.
 <br><br>
</details>

<details>
 <summary><html><body><i class="fas fa-exclamation-triangle"></i></body></html> <b>Have images been defaced?</b></summary><br>

 If you are sharing data with facial features, have these images been defaced and assessed for the quality of the defacing? Consider using fsl-deface. Consider using <a href="https://raamana.github.io/visualqc/gallery_defacing.html">VisualQC</a> to inspect and document the success of your defacing.
 <br><br>
</details>

## Metadata

<!-- ![cani-meta](../../../img/img-cani-meta.png) -->

<details>
 <summary><html><body><i class="fas fa-check-circle"></i></body></html> <b>Is your data FAIR?</b></summary><br>

 <p>FAIR data is findable, accessible, interoperable and reusable. Take a look at the <a href="https://faircookbook.elixir-europe.org/content/home.html">FAIR Cook Book</a> alongside these questions to make sure the data the you publish has the most value to our community.</p>

 <p><b>File formats</b> are an important feature of FAIR standards. In all cases you should aim to release your data in non-proprietary formats (for example comma separated values <code>csv</code> rather than excel <code>xsl</code>).</p>

 <p>Shared data should be <a href="https://en.wikipedia.org/wiki/Machine-readable_data">machine readable</a> where possible, and any non-imaging data should be provided in a <b>single file containing all measures</b> (for example covariates, behavioural measures, clinical outcomes). This data should be accompanied by a <b>data dictionary</b> which describes each of the variables included, how they were derived and where to obtain the source data where possible.</p>

 <br><br>
</details>

<details>
 <summary><html><body><i class="fas fa-check-circle"></i></body></html> <b>Have you conducted and prepared to share a quality control analysis?</b></summary><br>

 It is good practice to share a quality control (QC) analysis. Consider running <a href="https://mriqc.readthedocs.io/en/latest/">mriqc</a> and sharing the results with your data.
 <br><br>
</details>

<details>
 <summary><html><body><i class="fas fa-check-circle"></i></body></html> <b>Are you able to share the image acquisition protocol?</b></summary><br>

 Consider adding the MR protocol and scanning procedure documents to the <a href="https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/protocols/">MR Protocols database</a>. Add a link to your database entry digital object identifier (doi) in your shared data.
 <br><br>
</details>

<details>
 <summary><html><body><i class="fas fa-check-circle"></i></body></html> <b>Are behavioural and clinical covariates appropriately described?</b></summary><br>

 <p>Measured results for each participant should be provided in a single file containing all covariates, in appropriately <a href="https://en.wikipedia.org/wiki/Machine-readable_data">machine readable structure</a>.</p>
 <p>Covariates should be accompanied by a data dictionary which describes each of the variables included, how they were derived and the source data where possible.</p>

</details>

#### Community standards

<details>
 <summary><html><body><i class="fas fa-check-circle"></i></body></html> <b>Have you prepared the data according to community standards?</b></summary><br>

 <p>Sharing your data in accordance with community standards makes it easier for others to understand and work with your data. It also means that code developed to work on data structured to this standard will be easier to apply. </p>
 <p>The community standard for MRI data is the <a href="https://bids.neuroimaging.io">Brain Imaging Data Structure (BIDS)</a></p>
 <p>Community standards for other imaging data are evolving as <a href="https://bids.neuroimaging.io/get_involved.html#extending-the-bids-specification">BIDS extension proposals (BEPS)</a>. Take a look at the current BEPS and consider contributing to the development of a standard for your data type.</p>
 <p>A community standard for electrophysiology data is <a href="https://www.nwb.org">Neurodata Without Borders (NWB)</a>.</p>
</details>

<details>
 <summary><html><body><i class="fas fa-check-circle"></i></body></html> <b>Has the experimental protocol been described and made ready to share with the data?</b></summary><br>

 If you are following the BIDS standard, minimal experimental detail should be described in the dataset_descriptor.json file which is generated during BIDS conversion. If you are not following the BIDS standard, you should describe your experimental protocol to a sufficient level of detail and attach that information to your data.
 <br><br>
</details>

## Appropriate reuse

<!-- ![cani-reuse](../../../img/img-cani-reuse.png) -->

<!-- <html><body><i class="fas fa-exclamation-triangle"></i></body></html> -->
<!-- <html><body><i class="fas fa-check-circle"></i></body></html> -->

#### Access restrictions

<!-- Can access to your data be restricted in a way which protects the privacy of your participants and you intellectual work? -->

<details>
 <summary><html><body><i class="fas fa-check-circle"></i></body></html> <b>Can you create a "reviewer only" link to shared material?</b></summary><br>

 In some cases you may wish to make your data available only to a reviewer before making it available for wider release. Is this possible with your intended repository?
 <br><br>
</details>

<details>
 <summary><html><body><i class="fas fa-check-circle"></i></body></html> <b>Can you restrict access to bonafide researchers only?</b></summary><br>

 Given a the need for responsible reuse of your data, it may be wise to restrict re-use to those individuals who are likely to have a genuine research interest. Can your intended repository restrict access to allow only bonafide researchers, for example by institutional email verification, or an <a href="https://orcid.org">ORCID ID</a>?
 <br><br>
</details>


#### Your acknowledgement

<details>
 <summary><html><body><i class="fas fa-check-circle"></i></body></html> <b>Can you create a doi for your data?</b></summary><br>

 Does the tool you are using to share your data allow you to create a citable digital object identifier (doi) for the exact version of your data you are sharing? This doi can be used by others to reference your data.
 <br><br>
</details>

<details>
 <summary><html><body><i class="fas fa-check-circle"></i></body></html> <b>Can you select a license which requires attribution?</b></summary><br>

 Your data is a significant intellectual output, and you deserve to be recognised for it if your output is reused. We recommend using a repository where you can apply a license for reuse which necessitates attribution, for example <a href="https://creativecommons.org/licenses/by/4.0/">CC-BY-4.0</a>. You may additionally like to apply a license which restricts commercial use (for example <a href="https://creativecommons.org/licenses/by-nc/4.0/legalcode">CC-BY-NC-4.0</a>), allowing commercial use to be negotiated by the University.
 <br><br>
</details>

#### Customising your Data Usage Agreement (DUA)

<details>
  <summary><html><body><i class="fas fa-check-circle"></i></body></html> <b>Would you like to impose requirements for authorship?</b></summary><br>

  You may wish to stipulate that you are contacted to discuss authorship and further contributions if your data are reused. Alternatively you may wish to stipulate that you are not included as an author on any reuse of the data. Is it possible to impose such requirements with your intended repository?
  <br><br>
 </details>

 <details>
  <summary><html><body><i class="fas fa-check-circle"></i></body></html> <b>Would you like to impose restrictions on resharing?</b></summary><br>

  You may wish to stipulate that users of your data do not share it any further once they have acquired a copy. Is it possible to impose this requirement with your intended repository?
  <br><br>
 </details>

 <details>
  <summary><html><body><i class="fas fa-check-circle"></i></body></html> <b>Would you like to explicitly prohibit attempts to reidentify participants in your data?</b></summary><br>

  Given that many types of imaging can not be made fully anonymous, it may be wise to include an additional legal restriction which explicitly prohibits attempts to re-identify your participants, for example via linkage to other public sphere or experimental data. Is it possible to impose such requirements with your intended repository?
  <br><br>
 </details>

 <details>
  <summary><html><body><i class="fas fa-exclamation-triangle"></i></body></html> <b>Do you need to add any funder requirements in the reuse of your data?</b></summary><br>

  Some funders may require acknowledgement in perpetuity for data generated with their funds. Is it possible to impose such requirements with your intended repository?
  <br><br>
 </details>
