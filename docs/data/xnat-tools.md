---
layout: default
title: Tools for XNAT
parent: Open Data
has_children: true
nav_order: 5
---


# Tools relevant to sharing your data using XNAT
{: .fs-9 }

Stuff and things
{: .fs-6 .fw-300 }

---



#### Access
You can log into [WIN XNAT](https://xnat.win.ox.ac.uk) using your WIN IT account (currently requires a connection to the university network or VPN).

#### Useful background
The [XNAT](https://xnat.org) website has useful background information about the XNAT platform.

#### BIDS in XNAT
For the current overview of how BIDS works in XNAT, see the [BIDS](../bids) page.

#### Python libraries
There are several python libraries](data/python.md) that can be used to write scripts against the XNAT API.  See [python libraries](../python) for more info on pyxnat, xnatpy and dax.

#### Docker in XNAT
To see how Docker works in XNAT, see the [Docker](../docker) page.

#### Case Studies

The [OPDC project](../opdc) uploaded DICOM data from jalapeno using python and dcmtk.

TODO: More case studies!
