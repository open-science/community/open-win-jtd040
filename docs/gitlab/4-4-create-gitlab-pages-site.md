---
layout: default
title: 4.4 Create a GitLab Pages site
parent: Tutorials
grand_parent: Git and GitLab
has_children: false
nav_order: 14
---


# Create and publish your GitLab Pages site
{: .fs-8 }

Use the magic of Jekyll to transform markdown pages into a website
{: .fs-6 .fw-300 }

---

> **Note**
>
> This guide describes how to publish your project with **GitLab** Pages. If you are planning to publish a GitHub repo, see the [GitHub Pages Guide](../4-3-you-make-your-pages-site) instead!


## 1. Turn your repo into a GitLab Pages site!

Now the magic! Turn the simple collection of files made in [the previous tutorial](../4-1-you-make-your-repo) into something which looks like a user friendly website!


**Start from a WIN template**

To host your page on the WIN GitLab instance,

For a basic template based on [Jekyll](https://jekyllrb.com/), fork [this repository](https://git.fmrib.ox.ac.uk/open-science/community/gitlab_pages_jekyll_template) and then edit your copy of the template.

A second template using the *Beautiful Jekyll* theme can be forked [here](https://git.fmrib.ox.ac.uk/open-science/community/beautiful-jekyll).

A third template based on the stylish `just-the-docs` layout (particulalry suited for documentation, guides and manuals) can be forked [here](https://git.fmrib.ox.ac.uk/open-science/community/just-the-docs).

After forking one of the templates, remove the fork relationship by navigating to the forked project, then from the left panel select `Settings` > `General`. Find the panel `Advanced Settings` and click `Expand` > `Remove fork relationship`.


**Project name**

Update the project name of the forked template to something meaningful for your intended page. Go to `Settings` > `General` > `Project name`. Your project name should not contain any white spaces. Then change the path of your project to match your project name by going to `Settings` > `General` > `Advanced` > `Change path`.


**Configuration settings**

The file `_config.yml` contains the main settings to turn your project into a GitLab Pages site:

    * title: This is the title for your site
    * email: A contact email address for the page author/site operator
    * description: A description of the content of the site
    * baseurl: This is used to generate links to images etc, this should be set to /pages/<username>/<project name>
    * url: "https://open.win.ox.ac.uk" (This is the host address of the GitLab instance. Don't change this!)
    * version: If you wish to indicate that this is a new version of this site, increment this number

Note that the "project name" in the baseurl must match the name and path defined in the previous step.



An example `_config.yml` is shown below:

```
# Site settings
title: "Example Pages Template" # Provide a meaningful name
email: "myemail@ndcn.ox.ac.uk" # Set to contact email address
description: > # this means to ignore newlines until "baseurl:"
  Example GitLab Pages template
baseurl: "/pages/open-science/community/gitlab_pages_jekyll_template" # Set this to /pages/<group or username>/<project name>
url: "https://open.win.ox.ac.uk" # Leave this alone if publishing on WIN Pages site
version: "1.0"

# Build settings
markdown: kramdown

kramdown:
  input: GFM
  syntax_highlighter: rouge

plugins:
  - jekyll-sitemap
```



## 2. Deploying your site with CI

In order for GitLab to publish your site, you need to enable a "runner". Go to `Settings` > `CI/CD` > `Runners`. Tick `Enable shared runners`.

With the CI (continuous integration) runner enabled, every new commit that is pushed to your remote project directory will trigger a new build process. You can monitor the progress of the CI in `CI/CD` > `Pipelines` (rocket icon on the left navigation bar).

**Configuration settings**

The way CI is executed is based on a pipeline that runs a series of scripts that are grouped into "stages". Dividing the CI into different stages can help locate errors in the execution process. For example, you could set up *build*, *test* and *deploy* stages that each execute a set of shell commands and/or scripts. This is all specified in the configuration file `.gitlab-ci.yml`. However, it its simplest form, the file needs to contain only the following:

```
image: jekyll/jekyll
pages:
  script:
    - bundle exec jekyll build -d public
  tags:
    - docker
  artifacts:
    paths:
      - public
```

This tells the CI runner to use the jekyll Docker image and executes the jekyll build process. For more information, see the [gitlab docs](https://docs.gitlab.com/ee/ci/yaml/index.html#stages).



## 3. Viewing your site online

Assuming that the build process of the site completes without error, your GitLab Pages site will now be visable at:

    https://open.win.ox.ac.uk/pages/<your-username>/<your-project>



## 4. Troubleshooting

Additional information and more extensive documentation can be found at the [GitLab community help pages](https://git.fmrib.ox.ac.uk/help/user/project/pages/index).



## 5. Feedback

To provide feedback on the WIN templates, please use GitLab issues and merge requests in the respective project repositories!
