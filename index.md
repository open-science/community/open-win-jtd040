---
layout: default
title: Home
nav_order: 1
description: "Summary: Find out who we are, our aims for the community and how to get involved!"
has_children: true
---

<!-- ![WIN-logo](docs/img/WIN-h100.png) -->

# Open Research at the Wellcome Centre for Integrative Neuroimaging (WIN)
{: .fs-8 }

Find out how to share your research outputs effectively and responsibly, for improved impact, access and collaboration
{: .fs-6 .fw-300 }

---

This repository is the hub for information on how to employ open research practices at the [Wellcome Centre for Integrative Neuroimaging (WIN)](https://www.win.ox.ac.uk).

The material shared here is written and maintained by an active community of WIN researchers, and is intended to support all WIN members in using the sharing infrastructure created by the [WIN Open Neuroimaging Project](https://www.win.ox.ac.uk/open-neuroimaging).

**Jump straight into one of the sections below, or use the sidebar to find out more about this repository.**



<!-- Ambassadors highlight button -->
<!-- <br>
- <span class="fs-6">[Find out about the Open WIN Ambassadors Programme](../Open-WIN-Community/docs/community/ambassadors){: .btn .btn-green .mr-4 }</span> -->



<!-- Hero Unit -->
<section class="hero-unit">

  <div class="row">
    <div class="large-12 columns">
      <hgroup>
        <!-- <h1>Code. Design. Education.</h1> -->
        <h1></h1>
        <!-- <h3>Just keep learning. Make new opportunities.</h3> -->
        <h3></h3>
      </hgroup>


      <ul class="small-block-grid-2 medium-block-grid-3 flip-cards">

        <li ontouchstart="this.classList.toggle('hover');">
          <div class="large button card-front">
            <a href="https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/protocols"><nobr>MR Protocols</nobr></a>
            <!-- <i class="fa fa-code card-icon "></i> -->
            <i class="fa fa-magnet card-icon"></i>
          </div>
          <div class="panel card-back">
            <!-- <i class="fa fa-code card-icon"></i> -->
            <i class="fa fa-magnet card-icon"></i>
            <div class="hub-info">
              <a href="https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/protocols">More info</a>
              <p>How to share your magnetic resonance imaging protocols.</p>
            </div>
            <small class="clear"><a href="http://open.win.ox.ac.uk/protocols/">Use this tool</a></small>
          </div>
        </li>

        <li ontouchstart="this.classList.toggle('hover');">
          <div class="large button card-front">
            <a href="https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/tasks/">Tasks</a>
            <i class="fa fa-carrot card-icon"></i>
          </div>

          <div class="panel card-back">
            <i class="fa fa-carrot card-icon"></i>
            <div class="hub-info">
              <a href="https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/tasks/">More info</a>
              <p>How to share your experimental tasks.</p>
            </div>
            <small class="clear"><a href="https://git.fmrib.ox.ac.uk/open-science/tasks">Use this tool</a></small>
          </div>
        </li>

        <li ontouchstart="this.classList.toggle('hover');">
          <div class="large button card-front">
            <a href="https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/data/">Data</a>
            <i class="fa fa-chart-pie card-icon"></i>
          </div>

          <div class="panel card-back">
            <i class="fa fa-chart-pie card-icon"></i>
            <div class="hub-info">
              <a href="https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/data/">More info</a>
              <p>How to share your data responsibly.</p>
            </div>
            <!-- <small class="clear">Coming soon</small> -->
          </div>
        </li>

        <li ontouchstart="this.classList.toggle('hover');">
          <div class="large button card-front">
            <a href="https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/analysis/">Analysis</a>
            <i class="fa fa-code card-icon"></i>
          </div>

          <div class="panel card-back">
            <i class="fa fa-code card-icon"></i>
            <div class="hub-info">
              <a href="https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/analysis/">More info</a>
              <p>How to share reproducible FSL analysis pipelines.</p>
            </div>
            <!-- <small class="clear">Coming soon</small> -->
          </div>
        </li>

        <li ontouchstart="this.classList.toggle('hover');">
          <div class="large button card-front">
            <a href="https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/gitlab/"><nobr>Git and GitLab</nobr></a>
            <i class="fa fa-code-branch card-icon"></i>
          </div>

          <div class="panel card-back">
            <i class="fa fa-code-branch card-icon"></i>
            <div class="hub-info">
              <a href="https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/gitlab/">More info</a>
              <p>How to use the WIN GitLab instance for your code and documentation.</p>
            </div>
            <small class="clear"><a href="http://git.fmrib.ox.ac.uk/">Use this tool</a></small>
          </div>
        </li>

        <li ontouchstart="this.classList.toggle('hover');">
          <div class="large button card-front">
            <a href="https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/community/">Community</a>
            <i class="fa fa-users card-icon"></i>
          </div>

          <div class="panel card-back">
            <i class="fa fa-users card-icon"></i>
            <div class="hub-info">
              <a href="https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/community/">More info</a>
              <p>Find out about the Open WIN Community and how you can be a part of this effort</p>
            </div>
            <!-- <small class="clear">Updated the 4th Sunday.</small> -->
          </div>
        </li>

      </ul>
    </div>

    <!-- <div class="large-12 columns">
      <div class="small-12 small-centered medium-6 medium-centered large-3 large-centered columns clients">
        <a href="#">
          <h6 class="text-center">Clients Click Here</h6>
          <p class="text-center">
            <span class="fa-stack">
						<i class="fa fa-circle fa-stack-2x"></i>
						<i class="fa fa-angle-right fa-inverse fa-stack-1x"></i>
					</span>
          </p>
        </a>
      </div>
      <!-- end .clients </div> -->

  </div>
</section>



<!-- [![tools](img/btn-tools.png)](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/tools/)  [![community](img/btn-community.png)](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/community/)  [![ambassadors](img/btn-ambass.png)](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/ambassadors/)  [![events](img/btn-events.png)](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/events/)  [![gitlab](img/btn-git.png)](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/gitlab/) -->




<!-- [Open WIN Community](docs/community.md){: .btn .btn-primary .fs-6 .mb-4 .mb-md-0 .mr-2 }  
Find out about the Open WIN Community and how you can contribute

[Open WIN Ambassadors](docs/abmassadors.md){: .btn .btn-primary .fs-6 .mb-4 .mb-md-0 .mr-2 }  
Find out about our plans for the Open WIN Ambassadors scheme

[Open WIN Events](docs/events.md){: .btn .btn-primary .fs-6 .mb-4 .mb-md-0 .mr-2 }  
Look about for events relevant to our community! -->
